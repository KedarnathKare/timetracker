<?php
  require 'insertData.php';
  require 'retrieveData.php';  
  class EntryPoint{
  	static function main(){
  		$mode = $_REQUEST['mode'];
  		$data = $_REQUEST['data'];
  		if($mode == 'insert'){
  			$insertClass = new InsertData();
  			$insertClass->process($data);
  		}
  		if ($mode == 'retrieve') {
  			$retrieveClass = new RetrieveData();
  			$retrieveClass->process();
  		}
  	}
  }
  EntryPoint::main();
?>